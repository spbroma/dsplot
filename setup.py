from setuptools import setup

# git add . && git commit -m "commit_msg" && git push

setup(
    name='dsplot',
    version='0.1.15',
    author='Vlasov Roman',
    author_email='spbroma@gmail.com',
    packages=['dsplot'],
    # scripts=['bin/script1','bin/script2'],
    # url='http://pypi.python.org/pypi/PackageName/',
    # license='LICENSE.txt',
    description='Python wrapper around Matplotlib and Plotly with unified interface for efficient plotting of complex-valued data.',
    # long_description=open('README.txt').read(),
    install_requires=[
       "matplotlib",
       "numpy",
       "PyQt5",
       "scipy",
       "pyyaml"
    ],
)