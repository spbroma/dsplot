import numpy as np
from dsplot.support import plot_handler, plot_reim_i, plot_psd_i, \
    plot_xy_i, plot_abs_i, plot_amam_i, plot_ampm_i
import dsplot as dp
import logging
log = logging.getLogger(__name__)

# :param XXX (default - XXX): ZZZ

def reim(x, fig=None, new_fig=False, keep=True, **kwargs):
    '''
    Plots time-series data. If data contains complex part, it will be drawn as dashed line.

    :param x: input variable reim(x) or list of variables reim([x1, x2, x3])
    :param backend (default - 'matplotlib'): backend for plotting. Available: {'matplotlib', 'plotly'}. Plotly backend brings interactive plotting experience.
    :param style (default - 'solid', {'solid', 'dot'}): style of plotting
    :param fig (default - None): number of figure (for matplotlib backend)
    :param new (default - True): create new figure (for matplotlib backend)
    :param keep (default - False): keep data on current figure, if True, requires 'new' to be False (for matplotlib backend)
    :param show (default - True): if True, within function will be called plt.show() (for matplotlib backend)
    :param subplot (default - None): list of subplot configuration (for matplotlib backend)
    :param legend (default - None): list of legent labels, e.g. ['x1', 'x2', 'x3']
    :param colorscheme (default - cm.tab10): colorscheme for plots, look for more: https://matplotlib.org/stable/tutorials/colors/colormaps.html
    :param color_n (default - None): Number of colors, used from selected colorscheme. color_n = min(color_n, len(colorscheme)). If None, color_n = len(colorscheme)
    :param figsize (default - (10,6)): size of figure (for matplotlib backend)
    :param dpi (default - 100): DPI (dots per inch, resolution) of image. (for matplotlib backend)
    :param linewidth (default - 1): Width of line for plot
    :param lw_scale (default - True): If True, will increase width of lines when index of input item exceeds length of colortheme. For example, if n_color = 3, it will be [w1, w1, w1, w2, w2, w2, w3, w3, w3, ...]
    :param group_map (default - None): list features, by which plot will be colored. len(group_map) == len(x).
    :param unique_group_legend (default - True): if True, on legend will be only unique items of grouped data
    '''
    return plot_handler(x, plot_reim_i, fig=fig, new_fig=new_fig, keep=keep, **kwargs)


def plot(x, fig=None, new_fig=False, keep=True, **kwargs):
    '''
    Plots time-series data. If data contains complex part, it will be drawn as dashed line.

    :param x: input variable plot(x) or list of variables plot([x1, x2, x3])
    :param backend (default - 'matplotlib'): backend for plotting. Available: {'matplotlib', 'plotly'}. Plotly backend brings interactive plotting experience.
    :param style (default - 'solid', {'solid', 'dot'}): style of plotting
    :param fig (default - None): number of figure (for matplotlib backend)
    :param new (default - True): create new figure (for matplotlib backend)
    :param keep (default - False): keep data on current figure, if True, requires 'new' to be False (for matplotlib backend)
    :param show (default - True): if True, within function will be called plt.show() (for matplotlib backend)
    :param subplot (default - None): list of subplot configuration (for matplotlib backend)
    :param legend (default - None): list of legent labels, e.g. ['x1', 'x2', 'x3']
    :param colorscheme (default - cm.tab10): colorscheme for plots, look for more: https://matplotlib.org/stable/tutorials/colors/colormaps.html
    :param color_n (default - None): Number of colors, used from selected colorscheme. color_n = min(color_n, len(colorscheme)). If None, color_n = len(colorscheme)
    :param figsize (default - (10,6)): size of figure (for matplotlib backend)
    :param dpi (default - 100): DPI (dots per inch, resolution) of image. (for matplotlib backend)
    :param linewidth (default - 1): Width of line for plot
    :param lw_scale (default - True): If True, will increase width of lines when index of input item exceeds length of colortheme. For example, if n_color = 3, it will be [w1, w1, w1, w2, w2, w2, w3, w3, w3, ...]
    :param group_map (default - None): list features, by which plot will be colored. len(group_map) == len(x).
    :param unique_group_legend (default - True): if True, on legend will be only unique items of grouped data
    '''
    return plot_handler(x, plot_reim_i, fig=fig, new_fig=new_fig, keep=keep, **kwargs)


def psd(x, fig=None, new_fig=False, keep=True, **kwargs):
    '''Plots PSD (power spectral density) plot of input data.

    :param fs (default - 100): frequency sampling

    :param x: input variable psd(x) or list of variables psd([x1, x2, x3])
    :param backend (default - 'matplotlib'): backend for plotting. Available: {'matplotlib', 'plotly'}. Plotly backend brings interactive plotting experience.
    :param fig (default - None): number of figure (for matplotlib backend)
    :param new (default - True): create new figure (for matplotlib backend)
    :param keep (default - False): keep data on current figure, if True, requires 'new' to be False (for matplotlib backend)
    :param show (default - True): if True, within function will be called plt.show() (for matplotlib backend)
    :param subplot (default - None): list of subplot configuration (for matplotlib backend)
    :param legend (default - None): list of legent labels, e.g. ['x1', 'x2', 'x3']
    :param colorscheme (default - cm.tab10): colorscheme for plots, look for more: https://matplotlib.org/stable/tutorials/colors/colormaps.html
    :param color_n (default - None): Number of colors, used from selected colorscheme. color_n = min(color_n, len(colorscheme)). If None, color_n = len(colorscheme)
    :param figsize (default - (10,6)): size of figure (for matplotlib backend)
    :param dpi (default - 100): DPI (dots per inch, resolution) of image. (for matplotlib backend)
    :param linewidth (default - 1): Width of line for plot
    :param lw_scale (default - True): If True, will increase width of lines when index of input item exceeds length of colortheme. For example, if n_color = 3, it will be [w1, w1, w1, w2, w2, w2, w3, w3, w3, ...]
    :param group_map (default - None): list features, by which plot will be colored. len(group_map) == len(x).
    :param unique_group_legend (default - True): if True, on legend will be only unique items of grouped data
    '''
    return plot_handler(x, plot_psd_i, fig=fig, new_fig=new_fig, keep=keep, **kwargs)


def xy(x, fig=None, new_fig=False, keep=True, **kwargs):
    """Plots data with both coordinates, e.g. [[x1, y1], [x2, y2]].

    :param x: input variable xy([x1, y1]) or list of variables xy([[x1, y1], [x2, y2]])
    :param style (default - 'solid', {'solid', 'dot'}): style of plotting
    :param fig (default - None): number of figure (for matplotlib backend)
    :param new (default - True): create new figure (for matplotlib backend)
    :param keep (default - False): keep data on current figure, if True, requires 'new' to be False (for matplotlib backend)
    :param show (default - True): if True, within function will be called plt.show() (for matplotlib backend)
    :param subplot (default - None): list of subplot configuration (for matplotlib backend)
    :param legend (default - None): list of legent labels, e.g. ['x1', 'x2', 'x3']
    :param colorscheme (default - cm.tab10): colorscheme for plots, look for more: https://matplotlib.org/stable/tutorials/colors/colormaps.html
    :param color_n (default - None): Number of colors, used from selected colorscheme. color_n = min(color_n, len(colorscheme)). If None, color_n = len(colorscheme)
    :param figsize (default - (10,6)): size of figure (for matplotlib backend)
    :param dpi (default - 100): DPI (dots per inch, resolution) of image. (for matplotlib backend)
    :param linewidth (default - 1): Width of line for plot
    :param lw_scale (default - True): If True, will increase width of lines when index of input item exceeds length of colortheme. For example, if n_color = 3, it will be [w1, w1, w1, w2, w2, w2, w3, w3, w3, ...]
    :param group_map (default - None): list features, by which plot will be colored. len(group_map) == len(x).
    :param unique_group_legend (default - True): if True, on legend will be only unique items of grouped data
    """
    return plot_handler(x, plot_xy_i, fig=fig, new_fig=new_fig, keep=keep, **kwargs)


def abs(x, fig=None, new_fig=False, keep=True, **kwargs):
    '''
    Plots module (abs) of timeseries data.

    :param x: input variable abs(x) or list of variables abs([x1, x2, x3])
    :param fig (default - None): number of figure (for matplotlib backend)
    :param new (default - True): create new figure (for matplotlib backend)
    :param keep (default - False): keep data on current figure, if True, requires 'new' to be False (for matplotlib backend)
    :param show (default - True): if True, within function will be called plt.show() (for matplotlib backend)
    :param subplot (default - None): list of subplot configuration (for matplotlib backend)
    :param legend (default - None): list of legent labels, e.g. ['x1', 'x2', 'x3']
    :param colorscheme (default - cm.tab10): colorscheme for plots, look for more: https://matplotlib.org/stable/tutorials/colors/colormaps.html
    :param color_n (default - None): Number of colors, used from selected colorscheme. color_n = min(color_n, len(colorscheme)). If None, color_n = len(colorscheme)
    :param figsize (default - (10,6)): size of figure (for matplotlib backend)
    :param dpi (default - 100): DPI (dots per inch, resolution) of image. (for matplotlib backend)
    :param linewidth (default - 1): Width of line for plot
    :param lw_scale (default - True): If True, will increase width of lines when index of input item exceeds length of colortheme. For example, if n_color = 3, it will be [w1, w1, w1, w2, w2, w2, w3, w3, w3, ...]
    :param group_map (default - None): list features, by which plot will be colored. len(group_map) == len(x).
    :param unique_group_legend (default - True): if True, on legend will be only unique items of grouped data
    '''
    return plot_handler(x, plot_abs_i, fig=fig, new_fig=new_fig, keep=keep, **kwargs)


def amam(x, fig=None, new_fig=False, keep=True, **kwargs):
    '''
    Plots AM/AM characteristics

    :param x: input variable amam([x1, x2]) or list of variables amam([[x1, x2], [y1, y2]])
    :param fig (default - None): number of figure (for matplotlib backend)
    :param new (default - True): create new figure (for matplotlib backend)
    :param keep (default - False): keep data on current figure, if True, requires 'new' to be False (for matplotlib backend)
    :param show (default - True): if True, within function will be called plt.show() (for matplotlib backend)
    :param subplot (default - None): list of subplot configuration (for matplotlib backend)
    :param legend (default - None): list of legent labels, e.g. ['x1', 'x2', 'x3']
    :param colorscheme (default - cm.tab10): colorscheme for plots, look for more: https://matplotlib.org/stable/tutorials/colors/colormaps.html
    :param color_n (default - None): Number of colors, used from selected colorscheme. color_n = min(color_n, len(colorscheme)). If None, color_n = len(colorscheme)
    :param figsize (default - (10,6)): size of figure (for matplotlib backend)
    :param dpi (default - 100): DPI (dots per inch, resolution) of image. (for matplotlib backend)
    :param linewidth (default - 1): Width of line for plot
    :param lw_scale (default - True): If True, will increase width of lines when index of input item exceeds length of colortheme. For example, if n_color = 3, it will be [w1, w1, w1, w2, w2, w2, w3, w3, w3, ...]
    :param group_map (default - None): list features, by which plot will be colored. len(group_map) == len(x).
    :param unique_group_legend (default - True): if True, on legend will be only unique items of grouped data
    '''

    return plot_handler(x, plot_amam_i, fig=fig, new_fig=new_fig, keep=keep, **kwargs)


def ampm(x, fig=None, new_fig=False, keep=True, **kwargs):
    return plot_handler(x, plot_ampm_i, fig=fig, new_fig=new_fig, keep=keep, **kwargs)


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from dsplot.signal_gen import signal_gen

    N = 100000
    x = signal_gen(N, filt_order=512)
    y = x * np.conj(x)

    dp.psd([x, y])