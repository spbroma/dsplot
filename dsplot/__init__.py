from dsplot.functions import reim, psd, xy, abs, amam, ampm, plot
# import dsplot.utils as utils
from dsplot.utils import acpr, nmse, acpr_plot

import logging.config
import os
import json
import yaml


def my_function(
    default_path='logging.json',
    default_level=logging.INFO,
    env_key='LOG_CFG'
):
    """Setup logging configuration
    :meta public:
    """
    print('my_function')


# def setup_logging_json(
#     default_path='logging.json',
#     default_level=logging.WARNING,
#     env_key='LOG_CFG'
# ):
#     """Setup logging configuration
#
#     """
#     path = default_path
#     value = os.getenv(env_key, None)
#     if value:
#         path = value
#     if os.path.exists(path):
#         with open(path, 'rt') as f:
#             config = json.load(f)
#         logging.config.dictConfig(config)
#     else:
#         print('File not exist')
#         logging.basicConfig(level=default_level)


def setup_logging_yaml(
    default_path='logging.yaml',
    default_level=logging.WARNING,
    env_key='LOG_CFG'
):
    """Setup logging configuration

    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value

    cd = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(cd, path)

    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


setup_logging_yaml()
