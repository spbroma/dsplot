import matplotlib as mpl
# mpl.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D
import matplotlib.patheffects as path_effects

import tempfile
import pickle
import os

import logging
log = logging.getLogger(__name__)


class PlotObjectHandler:

    selected_line = None
    line = None
    tmp_path = os.path.join(tempfile.gettempdir(), 'tmp.pkl')
    ax = None
    pressed_keys = set()
    selected_lines = set()
    muted_lines = set()

    def __init__(self, ax=None):
        if not ax:
            self.ax = plt.gca()
        else:
            self.ax = ax

        fig = self.ax.get_figure()
        log.info(f'init fig num: {fig.number}')
        log.info(f'init fix ax: {self.ax}')
        fig.canvas.mpl_connect('key_press_event', self.key_press)
        fig.canvas.mpl_connect('key_release_event', self.key_release)
        fig.canvas.mpl_connect('pick_event', self.onpick1)

    def reset_effects(self):
        lines = plt.gca().lines
        for l in lines:
            l.set_path_effects([])
            l.set_alpha(np.min(1, l.get_alpha()*5))
            # l.set_alpha(1)


    def key_release(self, event):
        event.key = event.key.replace('ctrl', 'control')
        # print('release: ', event.key)
        if '+' in event.key:
            keys = event.key.split('+')
            self.pressed_keys.remove(keys[-1])
        else:
            self.pressed_keys.remove(event.key)
        # print('pressed keys: ', self.pressed_keys)

    def key_press(self, event):
        event.key = event.key.replace('ctrl', 'control')
        # print('press: ', event.key)
        if '+' in event.key:
            keys = event.key.split('+')
            for k in keys:
                self.pressed_keys.add(k)
        else:
            self.pressed_keys.add(event.key)

        # print('pressed keys: ', self.pressed_keys)

        if event.key == 'control+c':
            if len(self.selected_lines):
                with open(self.tmp_path, 'wb') as output:
                    pickle.dump(self.selected_lines, output)
                    log.info('Copied')
            else:
                log.info('Line is not selected')

        if event.key in ['delete', 'd']:
            if self.selected_lines:
                selected_lines_tmp = self.selected_lines.copy()
                for line in self.selected_lines:
                    if line in self.selected_lines:
                        selected_lines_tmp.remove(line)
                    if line in self.muted_lines:
                        self.muted_lines.remove(line)
                    line.remove()
                self.selected_lines = selected_lines_tmp
                self.ax.legend()
            else:
                log.info('Line is not selected')

        if event.key in ['control+v', 'control+V']:
            if os.path.isfile(self.tmp_path):

                with open(self.tmp_path, 'rb') as f:
                    lines = pickle.load(f)
                    plt.close(plt.gcf())

                    for line in lines:
                        self.ax.plot(line.get_data()[0], line.get_data()[1], picker=True)
                        self.ax.lines[-1].set_label(line.get_label())
                        if event.key == 'control+v':

                            self.ax.lines[-1].set_linestyle(line.get_linestyle())
                            self.ax.lines[-1].set_drawstyle(line.get_drawstyle())
                            self.ax.lines[-1].set_linewidth(line.get_linewidth())
                            self.ax.lines[-1].set_color(line.get_color())
                            self.ax.lines[-1].set_marker(line.get_marker())
                            self.ax.lines[-1].set_markersize(line.get_markersize())
                            self.ax.lines[-1].set_markerfacecolor(line.get_markerfacecolor())
                            self.ax.lines[-1].set_markeredgecolor(line.get_markeredgecolor())
                            self.ax.lines[-1].set_markerfacecoloralt(line.get_markerfacecoloralt())
                            self.ax.lines[-1].set_alpha(line.get_alpha())
                            self.ax.lines[-1].set_color(line.get_color())
                    self.ax.legend()

    def apply_effect_to_selected_line(self):
        self.reset_effects()
        for line in self.selected_lines:
            line.set_path_effects([path_effects.SimpleLineShadow(), path_effects.Normal()])

        for line in self.muted_lines:
            line.set_alpha(np.max(0.2, line.get_alpha()/5))

            # event.artist.set_path_effects([path_effects.Stroke(linewidth=2, foreground='black'), path_effects.Normal()])
            # https://matplotlib.org/3.1.1/tutorials/advanced/patheffects_guide.html

    def onpick1(self, event):
        # https://matplotlib.org/3.2.1/gallery/event_handling/pick_event_demo.html
        # if isinstance(event.artist, Line2D):
        if True:

            if self.pressed_keys == {'control'}:
                if event.artist in self.selected_lines:
                    self.selected_lines.remove(event.artist)
                else:
                    self.selected_lines.add(event.artist)

            if self.pressed_keys == {'alt'}:
                if event.artist in self.muted_lines:
                    self.muted_lines.remove(event.artist)
                else:
                    self.muted_lines.add(event.artist)

            if len(self.pressed_keys) == 0:
                # if len(self.selected_lines) == 1:
                if event.artist in self.selected_lines:
                    self.selected_lines.clear()
                else:
                    self.selected_lines = {event.artist}
                # else:

            self.apply_effect_to_selected_line()
            # print(event.artist)
            # print('selected: ', self.selected_lines)
            # print('muted: ', self.muted_lines)


if __name__ == '__main__':
    xArr = []
    if 1:
        for i in range(5):
            xArr.append(np.random.randn(100).cumsum())

    plt.figure()
    lines_arr =[]
    leg_arr = []
    for i, x in enumerate(xArr):
        l, = plt.plot(x, picker=True)
        lines_arr.append(l)
        leg_arr.append(f'x{i}')

        # plt.plot(x, picker=True, label=f'x{i}')

    plt.gca().legend(lines_arr, leg_arr)
    # plt.plot(x2, picker=True)
    # plt.legend(['x_'+str(i) for i in range(5)])
    # plt.gca().legend()
    plt.show()

    p = PlotObjectHandler()


