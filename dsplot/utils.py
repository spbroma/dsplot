import numpy as np
import scipy as sp
from scipy import signal, fft
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from collections.abc import Iterable


def nmse(x: np.ndarray, e: np.ndarray):
    """
    :param x:
    :param e:
    :return:
    """
    out = 10*np.log10(np.dot(e, np.conj(e)) / np.dot(x, np.conj(x)))
    return out


def acpr(x: np.ndarray,
         freq_main: float = 0.0,
         freq_adj: list = (-100.0, 100.0),
         band_main: float = 20.0,
         band_adj: list = (20.0, 20.0),
         fs: float = 100e6,
         nfft: int = 2048,
         show: bool = False):

    if len(freq_adj) != len(band_adj):
        print(f'Different length of freq_adj and band_adj: {len(freq_adj)}, {band_adj}')
        raise

    freq_psd, pxx = signal.welch(x, fs, return_onesided=False, nperseg=nfft)
    freq_psd = fft.fftshift(freq_psd)
    pxx = fft.fftshift(pxx)
    pxx = 10*np.log10(pxx)

    freq_main_0 = freq_main - band_main/2.0
    freq_main_1 = freq_main + band_main/2.0
    pwr_main = pxx[np.where((freq_main_0 < freq_psd) & (freq_psd < freq_main_1))]
    pwr_0 = np.mean(pwr_main)

    if show:
        plot_data = dict()
        plot_data['freq_psd'] = freq_psd
        plot_data['pxx'] = pxx
        plot_data['pwr_main'] = pwr_main
        plot_data['freq_main_0'] = freq_main_0
        plot_data['pwr_0'] = pwr_0
        plot_data['adj_data'] = []

        # plt.figure()
        # plt.plot(freq_psd/1e6, pxx)
        # ax = plt.gca()
        # [y_l, y_h] = ax.get_ylim()
        # y_h = np.max(pwr_main)
        # xy, w, h = (freq_main_0/1e6, y_l), band_main/1e6, (y_h-y_l)
        # ax.add_patch(plt.Rectangle(xy, w, h, linewidth=1, edgecolor='none', facecolor='b', alpha=0.2))
        # plt.text(freq_main/1e6, np.max(pwr_main), f'{pwr_0:.2f}', horizontalalignment='center', color='b')

    out = []
    for f, df in zip(freq_adj, band_adj):
        freq_0 = f - df / 2.0
        freq_1 = f + df / 2.0
        pwr_selected = pxx[np.where((freq_0 < freq_psd) & (freq_psd < freq_1))]
        pwr_i = np.mean(pwr_selected)
        acpr_i = pwr_i - pwr_0
        out.append(acpr_i)

        if show:

            y_h = np.max(pwr_selected)
            # xy, w, h = (freq_0/1e6, y_l), df/1e6, (y_h-y_l)
            # ax.add_patch(plt.Rectangle(xy, w, h, linewidth=1, edgecolor='none', facecolor='r', alpha=0.2))
            # plt.text(f/1e6, np.max(pwr_selected), f'{acpr_i:.2f}\n({pwr_i:.2f})', horizontalalignment='center', color='r')

            adj_data = dict()
            adj_data['y_h'] = y_h
            adj_data['acpr_i'] = acpr_i
            adj_data['pwr_i'] = pwr_i

            plot_data['adj_data'].append(adj_data)

    if show:
        return out, plot_data
    else:
        return out

        # plt.grid()
        # plt.xlabel('Frequency, MHz')
        # plt.ylabel('Power, dB')
        # plt.title('ACPR visualization')
        # plt.show()

    # return out


def acpr_plot(x: np.ndarray,
         freq_main: float = 0.0,
         freq_adj: list = (-100.0, 100.0),
         band_main: float = 20.0,
         band_adj: list = (20.0, 20.0),
         fs: float = 100e6,
         nfft: int = 2048,
         show: bool = True,
         figsize: tuple = (8, 5),
         dpi: int = 100):

    if not type(band_adj) == list:
        band_adj = list(band_adj)

    if len(band_adj) == 1 and len(freq_adj) > 1:
        band_adj = list(band_adj) * len(freq_adj)
    
    if show:
        acpr_out, plot_data = acpr(x, freq_main, freq_adj, band_main, band_adj, fs, nfft, show)

        freq_psd = plot_data['freq_psd']
        pxx = plot_data['pxx']
        pwr_main = plot_data['pwr_main']
        freq_main_0 = plot_data['freq_main_0']
        pwr_0 = plot_data['pwr_0']
        adj_data = plot_data['adj_data']

        plt.figure(figsize=figsize, dpi=dpi)
        plt.plot(freq_psd/1e6, pxx)
        ax = plt.gca()
        [y_l, y_h] = ax.get_ylim()
        y_h = np.max(pwr_main)
        xy, w, h = (freq_main_0/1e6, y_l), band_main/1e6, (y_h-y_l)
        ax.add_patch(plt.Rectangle(xy, w, h, linewidth=1, edgecolor='none', facecolor='b', alpha=0.2))
        plt.text(freq_main/1e6, np.max(pwr_main), f'{pwr_0:.2f}', horizontalalignment='center', color='b')

        for f, df, data in zip(freq_adj, band_adj, adj_data):

            y_h = data['y_h']
            acpr_i = data['acpr_i']
            pwr_i = data['pwr_i']

            freq_0 = f - df / 2.0
            # freq_1 = f + df / 2.0

            xy, w, h = (freq_0/1e6, y_l), df/1e6, (y_h-y_l)
            ax.add_patch(plt.Rectangle(xy, w, h, linewidth=1, edgecolor='none', facecolor='r', alpha=0.2))
            plt.text(f/1e6, y_h, f'{acpr_i:.2f}\n({pwr_i:.2f})', horizontalalignment='center', color='r')

        plt.grid()
        plt.xlabel('Frequency, MHz')
        plt.ylabel('Power, dB')
        plt.title('ACPR visualization')
        plt.show()
    else:
        acpr_out = acpr(x, freq_main, freq_adj, band_main, band_adj, fs, nfft, show)

    return acpr_out
    


