import matplotlib.patheffects as path_effects
import matplotlib.pyplot as plt
import tempfile
import pickle
import os
import logging
log = logging.getLogger(__name__)


class ZoomPanSelect:
    def __init__(self, colorscheme=None, clr_ind=None):
        self.press = None
        self.cur_xlim = None
        self.cur_ylim = None
        self.x0 = None
        self.y0 = None
        self.x1 = None
        self.y1 = None
        self.xpress = None
        self.ypress = None
        self.active_ax = 'xy'

        self.pressed_keys = set()
        self.selected_lines = set()
        self.muted_lines = set()
        self.tmp_path = os.path.join(tempfile.gettempdir(), 'tmp.pkl')

        self.alphas = []
        self.colorscheme = colorscheme
        self.clr_ind = clr_ind

        lines = plt.gca().lines
        for l in lines:
            self.alphas.append(l.get_alpha())

    def select_factory(self, ax):
        def reset_effects():
            lines = ax.lines
            for l, a in zip(lines, self.alphas):
                l.set_path_effects([])
                l.set_alpha(a)

        def apply_effect_to_selected_line():
            reset_effects()
            for line in self.selected_lines:
                # line.set_path_effects([path_effects.SimpleLineShadow(), path_effects.Normal()])
                # line.set_path_effects([path_effects.Stroke(linewidth=2, foreground='black'), path_effects.Normal()])
                lw = line.get_lw()
                line.set_path_effects([path_effects.Stroke(linewidth=lw + 1.5, foreground='black'),
                                       path_effects.Stroke(linewidth=lw + 0.5, foreground='white'),
                                       path_effects.Normal()])
                # line.set_path_effects([path_effects.Normal(),
                #                        path_effects.Stroke(linewidth=0.5, foreground='white')])
            for line in self.muted_lines:
                line.set_alpha(0.2)

        def onpick1(event):
            # log.info(event)
            # https://matplotlib.org/3.2.1/gallery/event_handling/pick_event_demo.html

            if self.pressed_keys == {'control'}:
                if event.artist in self.selected_lines:
                    self.selected_lines.remove(event.artist)
                else:
                    self.selected_lines.add(event.artist)

            if self.pressed_keys == {'shift'}:
                if event.artist in self.muted_lines:
                    self.muted_lines.remove(event.artist)
                else:
                    self.muted_lines.add(event.artist)

            if len(self.pressed_keys) == 0:
                if event.artist in self.selected_lines:
                    self.selected_lines.clear()
                else:
                    self.selected_lines = {event.artist}

            apply_effect_to_selected_line()
            log.info(event.artist)
            # log.info('selected: ', self.selected_lines)
            # log.info('muted: ', self.muted_lines)

        def key_release(event):
            event.key = event.key.replace('ctrl', 'control')
            log.info(f'release: {event.key}')
            key_to_remove = None
            if '+' in event.key:
                keys = event.key.split('+')
                key_to_remove = keys[-1]
            else:
                key_to_remove = event.key

            if key_to_remove in self.pressed_keys:
                self.pressed_keys.remove(key_to_remove)
            log.info(f'pressed keys: {self.pressed_keys}')

        def key_press(event):
            event.key = event.key.replace('ctrl', 'control')
            log.info(f'press: {event.key}')
            if '+' in event.key:
                keys = event.key.split('+')
                for k in keys:
                    if k != 'alt':
                        self.pressed_keys.add(k)
            else:
                k = event.key
                if k != 'alt':
                    self.pressed_keys.add(k)

            log.info(f'pressed keys: {self.pressed_keys}')

            if event.key == 'control+c':
                if len(self.selected_lines):
                    with open(self.tmp_path, 'wb') as output:
                        pickle.dump(self.selected_lines, output)
                        log.info('Copied')
                else:
                    log.info('Line is not selected')

            if event.key in ['delete', 'd']:
                if self.selected_lines:
                    selected_lines_tmp = self.selected_lines.copy()
                    for line in self.selected_lines:
                        if line in self.selected_lines:
                            selected_lines_tmp.remove(line)
                        if line in self.muted_lines:
                            self.muted_lines.remove(line)
                        line.remove()
                    self.selected_lines = selected_lines_tmp
                    ax.legend()
                else:
                    log.info('Line is not selected')

            if event.key in ['control+v', 'control+V']:
                if os.path.isfile(self.tmp_path):

                    with open(self.tmp_path, 'rb') as f:
                        lines = pickle.load(f)
                        plt.close(plt.gcf())

                        for line in lines:
                            ax.plot(line.get_data()[0], line.get_data()[1], picker=True)
                            ax.lines[-1].set_label(line.get_label())
                            ax.lines[-1].set_linewidth(line.get_linewidth())

                            if event.key == 'control+v':
                                ax.lines[-1].set_linestyle(line.get_linestyle())
                                ax.lines[-1].set_drawstyle(line.get_drawstyle())
                                ax.lines[-1].set_color(line.get_color())
                                ax.lines[-1].set_marker(line.get_marker())
                                ax.lines[-1].set_markersize(line.get_markersize())
                                ax.lines[-1].set_markerfacecolor(line.get_markerfacecolor())
                                ax.lines[-1].set_markeredgecolor(line.get_markeredgecolor())
                                ax.lines[-1].set_markerfacecoloralt(line.get_markerfacecoloralt())
                                ax.lines[-1].set_alpha(line.get_alpha())
                                ax.lines[-1].set_color(line.get_color())
                            else:
                                if (self.colorscheme is not None) and (self.clr_ind is not None):
                                    self.clr_ind += 1
                                    self.clr_ind = int(self.clr_ind % self.colorscheme.N)
                                    clr = list(self.colorscheme(self.clr_ind))
                                    ax.lines[-1].set_color(clr)

                        ax.legend()

            if event.key == 'escape':
                self.selected_lines = set()
                self.muted_lines = set()
                apply_effect_to_selected_line()

            plt.draw()

        fig = ax.get_figure()
        fig.canvas.mpl_connect('pick_event', onpick1)
        fig.canvas.mpl_connect('key_press_event', key_press)
        fig.canvas.mpl_connect('key_release_event', key_release)

    def zoom_factory(self, ax, base_scale=2.):
        def zoom(event):
            cur_xlim = ax.get_xlim()
            cur_ylim = ax.get_ylim()

            if event.inaxes == ax:

                if event.key == 'shift':
                    active_ax = 'x'
                elif event.key == 'control':
                    active_ax = 'y'
                else:
                    active_ax = 'xy'

                xdata = event.xdata # get event x location
                ydata = event.ydata # get event y location

                if event.button == 'up':
                    # deal with zoom in
                    scale_factor = 1 / base_scale
                elif event.button == 'down':
                    # deal with zoom out
                    scale_factor = base_scale
                else:
                    # deal with something that should never happen
                    scale_factor = 1

                new_width = (cur_xlim[1] - cur_xlim[0]) * scale_factor
                new_height = (cur_ylim[1] - cur_ylim[0]) * scale_factor

                relx = (cur_xlim[1] - xdata)/(cur_xlim[1] - cur_xlim[0])
                rely = (cur_ylim[1] - ydata)/(cur_ylim[1] - cur_ylim[0])

                if active_ax == 'x' or active_ax == 'xy':
                    ax.set_ylim([ydata - new_height * (1-rely), ydata + new_height * (rely)])
                if active_ax == 'y' or active_ax == 'xy':
                    ax.set_xlim([xdata - new_width * (1-relx), xdata + new_width * (relx)])
                ax.figure.canvas.draw()

        fig = ax.get_figure() # get the figure of interest
        fig.canvas.mpl_connect('scroll_event', zoom)

        return zoom

    def pan_factory(self, ax):
        def onPress(event):
            if event.inaxes == ax:

                if event.dblclick:
                    log.info('doubleclick')
                    ax.autoscale(enable=True)
                else:
                    if event.button == 3:
                        if event.key == 'shift':
                            self.active_ax = 'y'
                        elif event.key == 'control':
                            self.active_ax = 'x'
                        else:
                            self.active_ax = 'xy'

                        self.cur_xlim = ax.get_xlim()
                        self.cur_ylim = ax.get_ylim()
                        self.press = self.x0, self.y0, event.xdata, event.ydata
                        self.x0, self.y0, self.xpress, self.ypress = self.press

        def onRelease(event):
            self.press = None
            ax.figure.canvas.draw()

        def onMotion(event):
            if self.press is None: return
            if event.inaxes != ax: return

            if self.active_ax == 'x' or self.active_ax == 'xy':
                dx = event.xdata - self.xpress
                self.cur_xlim -= dx
                ax.set_xlim(self.cur_xlim)

            if self.active_ax == 'y' or self.active_ax == 'xy':
                dy = event.ydata - self.ypress
                self.cur_ylim -= dy
                ax.set_ylim(self.cur_ylim)

            ax.figure.canvas.draw()

        fig = ax.get_figure() # get the figure of interest

        # attach the call back
        fig.canvas.mpl_connect('button_press_event',onPress)
        fig.canvas.mpl_connect('button_release_event',onRelease)
        fig.canvas.mpl_connect('motion_notify_event',onMotion)
        # fig.canvas.mpl_connect('key_press_event',onBtnPress)
        # fig.canvas.mpl_connect('key_release_event',onBtnRelease)

        #return the function
        return onMotion


if __name__ == '__main__':
    import numpy
    from matplotlib.pyplot import figure, show
    import matplotlib.pyplot as plt
    fig = figure()

    # ax = fig.add_subplot(111, xlim=(0, 1), ylim=(0, 1), autoscale_on=False)

    # ax.set_title('Click to zoom')
    x, y, s, c = numpy.random.rand(4, 200)
    s *= 200

    # ax.scatter(x, y, s, c)
    plt.figure()
    plt.subplot(2,1,1)
    plt.scatter(x, y, s, c)

    ax = plt.gca()
    scale = 1.1
    zp = ZoomPanSelect()
    figZoom = zp.zoom_factory(ax, base_scale=scale)
    figPan = zp.pan_factory(ax)

    plt.subplot(2,1,2)
    plt.scatter(x, y, s, c)

    ax = plt.gca()
    scale = 1.1
    zp = ZoomPanSelect()
    figZoom = zp.zoom_factory(ax, base_scale=scale)
    figPan = zp.pan_factory(ax)

    show()
