import matplotlib as mpl

try:
    mpl.use('Qt5Agg')  # [ Qt5Agg, TkAgg ]
except:
    print('Couldn''t set Qt5 backend')

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from dsplot.zoom_pan_select import ZoomPanSelect
import logging
from collections.abc import Iterable
from scipy import signal, fft

log = logging.getLogger(__name__)

# plt.ion()

try:
    import plotly.graph_objects as go
    # import plotly.io as pio
    # pio.renderers.default = 'browser'
except:
    pass


def mplclr2hex(clr):
    '''
    Convert color from [r, g, b, a] to "#RRGGBB"
    :param clr:
    :return: clr string
    '''
    clr = (np.array(clr)*255).astype('uint8')[:3]
    clr_ = '#'
    for c in clr:
        # clr_ += hex(c)[2:]
        clr_ += '{0:0{1}X}'.format(c,2)
    return clr_


def plot_xy_i(x: list, clr, label=None, lw=1, **kwargs):
    cmpx_flag = np.iscomplex(x).any()
    if cmpx_flag:
        label_re = ' (re)'
    else:
        label_re = ''

    # Style
    style = kwargs['style']
    backend = kwargs['backend']
    fig = kwargs['fh']

    if style == 'solid':
        ps_re = '-'
        ps_im = '--'
    elif style == 'dot':
        ps_re = '.'
        ps_im = '2'
    else:
        log.error(f'Invalid style: {style}')
        raise

    # Transparancy
    alpha = kwargs['alpha']

    if backend == 'matplotlib':

        if label:
            plt.plot(x[0].real, x[1].real, ps_re, color=clr, picker=True, label=label + label_re, lw=lw, alpha=alpha)
            if cmpx_flag:
                plt.plot(x[0].imag, x[1].imag, ps_im, color=clr, picker=True, label=label + ' (im)', lw=lw, alpha=alpha)
        else:
            plt.plot(x[0].real, x[1].real, ps_re, color=clr, picker=True, lw=lw, alpha=alpha)
            if cmpx_flag:
                plt.plot(x[0].imag, x[1].imag, ps_im, color=clr, picker=True, lw=lw, alpha=alpha)
        xmax = max(x[0].real.max(), x[0].imag.max())
        xmin = min(x[0].real.min(), x[0].imag.min())

        plt.xlim(xmin, xmax)

    elif backend == 'plotly':
        clr = mplclr2hex(clr)
        if label:
            fig.add_trace(go.Scatter(x=x[0].real, y=x[1].real, name=label + label_re, opacity=alpha, line=dict(color=clr, width=lw)))
            if cmpx_flag:
                fig.add_trace(go.Scatter(x=x[0].imag, y=x[1].imag, name=label + ' (im)', opacity=alpha, line=dict(color=clr, width=lw, dash='dash')))
        else:
            fig.add_trace(go.Scatter(x=x[0].real, y=x[1].real, opacity=alpha, line=dict(color=clr, width=lw)))
            if cmpx_flag:
                fig.add_trace(go.Scatter(x=x[0].imag, y=x[1].imag, opacity=alpha, line=dict(color=clr, width=lw, dash='dash')))

    return fig


def plot_amam_i(x: list, clr, label=None, lw=1, **kwargs):
    x_x = np.abs(x[0]).astype(np.float32)
    x_y = np.abs(x[1]).astype(np.float32)

    # Transparancy
    alpha = kwargs['alpha']

    if label:
        plt.scatter(x_x, x_y, color=clr, picker=True, label=label, s=lw, alpha=alpha)
    else:
        plt.scatter(x_x, x_y, color=clr, picker=True, s=lw, alpha=alpha)


def plot_ampm_i(x: list, clr, label=None, lw=1, **kwargs):
    x_x = np.abs(x[0]).astype(np.float32)
    x_y = np.angle(x[0] * np.conj(x[1])).astype(np.float32)

    # Transparancy
    alpha = kwargs['alpha']

    if label:
        plt.scatter(x_x, x_y, color=clr, picker=True, label=label, lw=lw, alpha=alpha)
    else:
        plt.scatter(x_x, x_y, color=clr, picker=True, lw=lw, alpha=alpha)


def plot_reim_i(x, clr, xmax=None, label=None, lw=1, **kwargs):
    cmpx_flag = np.iscomplex(x).any()
    if cmpx_flag:
        label_re = ' (re)'
    else:
        label_re = ''

    marker = kwargs['marker']
    fig = kwargs['fh']
    backend = kwargs['backend']

    # Style
    style = kwargs['style']
    if style == 'solid':
        ps_re = '-'
        ps_im = '--'
    elif style == 'dot':
        ps_re = '.'
        ps_im = '2'
    else:
        log.error(f'Invalid style: {style}')
        raise

    # Transparancy
    alpha = kwargs['alpha']

    if backend == 'matplotlib':

        if label:
            plt.plot(x.real, ps_re, color=clr, picker=True, label=label + label_re, lw=lw, alpha=alpha)
            if cmpx_flag:
                plt.plot(x.imag, ps_im, color=clr, picker=True, label=label + ' (im)', lw=lw, alpha=alpha)
        else:
            plt.plot(x.real, ps_re, color=clr, picker=True, lw=lw, alpha=alpha)
            if cmpx_flag:
                plt.plot(x.imag, ps_im, color=clr, picker=True, lw=lw, alpha=alpha)

        if xmax is None:
            xmax = len(x)
        xmin = 0
        plt.xlim(xmin, xmax)

    elif backend == 'plotly':
        clr = mplclr2hex(clr)
        if label:
            fig.add_trace(go.Scatter(y=x.real, name=label + label_re, opacity=alpha, line=dict(color=clr, width=lw)))
            if cmpx_flag:
                fig.add_trace(go.Scatter(y=x.imag, name=label + ' (im)', opacity=alpha, line=dict(color=clr, width=lw, dash='dash')))
        else:
            fig.add_trace(go.Scatter(y=x.real, opacity=alpha, line=dict(color=clr, width=lw)))
            if cmpx_flag:
                fig.add_trace(go.Scatter(y=x.imag, opacity=alpha, line=dict(color=clr, width=lw, dash='dash')))

    return fig


def plot_abs_i(x, clr, xmax=None, label=None, lw=1, **kwargs):
    x = np.abs(x)

    # Transparancy
    alpha = kwargs['alpha']

    if label:
        plt.plot(x.real, color=clr, picker=True, label=label, lw=lw, alpha=alpha)
    else:
        plt.plot(x.real, color=clr, picker=True, lw=lw, alpha=alpha)

    if xmax is None:
        xmax = len(x)
    xmin = 0
    plt.xlim(xmin, xmax)


def plot_psd_i(x, clr, fs, nfft=2048, label=None, lw=1, oneside=None, **kwargs):

    backend = kwargs['backend']
    fig = kwargs['fh']

    # Transparancy
    alpha = kwargs['alpha']

    if oneside is None:
        oneside = not np.iscomplexobj(x)

    f, Pxx_den = signal.welch(x, fs, nperseg=nfft, return_onesided=oneside)

    if not oneside:
        f = fft.fftshift(f)
        Pxx_den = fft.fftshift(Pxx_den)

    Pxx_den = 10*np.log10(Pxx_den)

    if backend == 'matplotlib':
        if label:
            # plt.psd(x, color=clr, Fs=fs, NFFT=nfft, linewidth=lw, picker=True, label=label, alpha=alpha)
            plt.plot(f, Pxx_den, linewidth=lw, picker=True, label=label, alpha=alpha)
        else:
            # plt.psd(x, color=clr, Fs=fs, NFFT=nfft, linewidth=lw, picker=True, alpha=alpha)
            plt.plot(f, Pxx_den, linewidth=lw, picker=True, alpha=alpha)
        plt.xlabel('Frequency, Hz')

    elif backend == 'plotly':
        clr = mplclr2hex(clr)

        if label:
            fig.add_trace(go.Scatter(x=f, y=Pxx_den, name=label, line=dict(color=clr, width=lw), opacity=alpha))
        else:
            fig.add_trace(go.Scatter(x=f, y=Pxx_den, line=dict(color=clr, width=lw), opacity=alpha))

    return fig


def is_not_list_ndarray(x):
    return (type(x) is not np.ndarray) and \
           (type(x) is not list)


def cast_to_ndarray(x):
    if isinstance(x, Iterable):
        x = list(x)
        if isinstance(x[0], Iterable):
            for i in range(len(x)):
                x[i] = cast_to_ndarray(x[i])
        else:
            x = np.array(x)
    return x


def check_two_axes_data(x):
    for xi in x:
        if len(xi[0]) != len(xi[1]):
            assert 'Non-equal length of data'


def contains_list_of_plots(x, plot_func):
    if is_two_axis_function(plot_func):
        check_two_axes_data(x)
        return True
        # return isinstance(x[0], Iterable) and (len(x[0].shape) > 2)
    else:
        if isinstance(x, Iterable):
            return isinstance(x[0], Iterable)
        else:
            return False


def is_two_axis_function(f):
    return f.__name__.__contains__('xy') or \
           f.__name__.__contains__('amam') or \
           f.__name__.__contains__('ampm')


def plot_handler(x, plot_func, fig=None, new_fig=False, keep=True,
                 subplot=None, fs=100, legend=None,
                 colorscheme=cm.tab10, color_n=None,
                 figsize=(8, 5), dpi=100, linewidth=1, lw_scale=True,
                 show=False, new=False,
                 group_map=None, unique_group_legend=True,
                 backend='matplotlib', style='solid', alpha=1, 
                 hover_label_len=-1, hover_format='.3f', legend_outside=False,
                 **kwargs):
    plt.rc('font', size=8)

    x = cast_to_ndarray(x)
    if not contains_list_of_plots(x, plot_func):
        log.info(f'not contains_iterable')
        x = [x]

    new_fig = new
    if type(subplot) == list:
        new_fig = False
        keep = True
        plt.subplot(subplot[0], subplot[1], subplot[2])

    if backend == 'matplotlib':
        if new_fig:
                plt.figure(fig, figsize=figsize, dpi=dpi)

        if not keep:
            plt.gca().cla()

    xmax = 0

    if (color_n is None) or (color_n > colorscheme.N):
        clr_n = colorscheme.N
    else:
        clr_n = color_n

    if group_map is not None:
        group_map = np.array(group_map)
        group_map_set = set(list(group_map))
        clr_unique = np.unique(group_map)
        legend = group_map
    else:
        group_map_set = set()
        clr_unique = []

    if backend == 'matplotlib':
        fh = None
    elif backend == 'plotly':
        fh = go.Figure()
    else:
        fh = None
        log.error('Invalid backend')
        raise Exception('Invalid backend. Available: "matplotlib", "plotly"')

    for i in range(len(x)):
        xi = x[i]
        if not isinstance(xi, Iterable):
            xi = np.array([xi])
            marker = 'o'
        else:
            marker = ''

        show_in_legend = True
        if group_map is not None:
            if group_map[i] in group_map_set:
                if unique_group_legend:
                    group_map_set.remove(group_map[i])
            else:
                show_in_legend = False
            clr_ind = np.where(clr_unique == group_map[i])[0][0]
        else:
            clr_ind = int(i % clr_n)

        if lw_scale:
            lw = np.ceil((i + 1) / clr_n) * linewidth
        else:
            lw = linewidth

        clr = list(colorscheme(clr_ind))
        if len(xi) > xmax:
            xmax = len(xi)

        label = None
        if legend is not None:
            if i < len(legend) and show_in_legend:
                label = str(legend[i])

        args = {'x': xi, 'clr': clr, 'fs': fs, 'xmax': xmax,
                'label': label, 'lw': lw, 'marker': marker,
                'fh': fh, 'backend': backend, 'style': style,
                'alpha': alpha}
        args.update(kwargs)
        fh = plot_func(**args)

    if backend == 'matplotlib':
        plt.grid(True)

        ax = plt.gca()
        if legend is not None:
            if legend_outside:
                ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            else:
                ax.legend()

        zp = ZoomPanSelect(colorscheme, clr_ind)
        zp.zoom_factory(ax, base_scale=1.1)
        zp.pan_factory(ax)
        zp.select_factory(ax)

    # if show:
    #     if backend == 'matplotlib':
    #         plt.show()
    #     elif backend == 'plotly':
    #         fh.update_layout(template='none')  # ["plotly", "plotly_white", "plotly_dark", "ggplot2", "seaborn", "simple_white", "none"]
    #         fh.update_layout(hovermode='closest')  # ["x" | "y" | "closest" | False | "x unified" | "y unified" ]
    #         fh.update_layout(hoverlabel_namelength=hover_label_len)
    #         fh.update_yaxes(hoverformat=hover_format)
    #         fh.update_xaxes(showline=True, mirror=True, gridwidth=1, gridcolor='#999999')  # https://plotly.com/python/axes/#toggling-axis-zero-lines
    #         fh.update_yaxes(showline=True, mirror=True, gridwidth=1, gridcolor='#999999')

    #         fh.show()
    #         # print('fh')
    #         # return fh
    #         # fig.show(renderer='browser')
    
    if backend == 'matplotlib':
        return plt.gcf()
    elif backend == 'plotly':
        return fh
    



