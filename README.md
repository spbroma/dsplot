# DSPLOT
Dsplot - is abbreviation for "DSP plot". 

It is Python wrapper around Matplotlib and Plotly with unified interface for comfortable plotting of complex-valued data.

Initial idea was to bring some Matlab plotting experience to Matplotlib in Python, e.g., some interactive opportunities like selecting and copying plots, panning and zooming figure.

![image](doc/dsplot_demo_3.gif)

These features are available only with Matplotlib backend in window mode (when you run script in console and figure opens in PyQt window). 

In Jupyter Notebook Matplotlib backend is static. So, to bring interactivity to Jupyter mode was added Plotly backend.

# Usage
## Install
During installation will be installed following packages:
- PyQt5 - for better experience in window mode with Matplotlib backend
- Plotly - for interactive Jupyter plotting
### Variant 1
```bash
pip install git+https://gitlab.com/spbroma/dsplot.git
```
### Variant 2
```bash
git clone https://gitlab.com/spbroma/dsplot.git
pip install dsplot
```
## Import
```python
import dsplot as dp
```


## dp.plot - time series complex plotting 
```python
dp.plot([x1, x2, x3], legend=['x1', 'x2', 'x3'])
```
If data is real, it will plot one solid line. Otherwise, you will see solid line for real and dashed for image part.
![image](doc/plot.png)

## dp.psd - power spectral density
Function-specific arguments:

- `fs` (default: 100e6) - frequency sampling

```python
dp.psd([x1, x2], legend=['x1', 'x2'])
```
![image](doc/psd.png)

<!-- ## dp.xy - two-coordinate plotting -->
