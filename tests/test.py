import dsplot as dp
from dsplot.signal_gen import signal_gen
import numpy as np

import plotly.io as pio
import scipy.io as sio
# pio.renderers.default = 'browser'

mat = sio.loadmat('C:\\Users\\spbro\\Documents\\python\\tmp\\BlackBoxData\\BlackBoxData_80.mat')
eRef = mat['eRef'][0]
x = mat['x'][0]
d = mat['y'][0]


# dp.psd([x, d, eRef], backend='matplotlib')


# N = 100000
# x = signal_gen(N, filt_order=512)
# y = x * np.conj(x)
#
# dp.psd([x, y])

# N = 1000
# x = signal_gen(N, filt_order=512)
# y = x * np.conj(x)
#
#
# # dp.plot([x, y], backend='plotly', legend=['plot a', 'plot b'])
# dp.plot([x, y], legend=['plot a', 'plot b'])


import matplotlib.pyplot as plt

import dsplot as dp
from dsplot.signal_gen import signal_gen
import pandas as pd

# dp.xy([[[1,2], [3,4]], [[1,2,3], [5,6,7]]], backend='plotly')



N = 5e5
x = signal_gen(N, filt_order=1024)

x += x * abs(x)

# x_list = []
# xy_list = []
#
# x = x * 1.2
# x_list.append(x)
# for i in range(4):
#     x = x * 1.2
#     # x_list.append(x.real)
#     xi = signal_gen(N*(i+1))
#     if i%2 == 0:
#         xi = xi.real
#     x_list.append(xi)
#
# df = pd.DataFrame([])
# df['x'] = x_list
#
# for i in range(3):
#     xy_list.append((signal_gen(N*(i+1)), signal_gen(N*(i+1))))

# dp.psd(x)

out = dp.acpr(x, fs=300e6,
        freq_main=10e6,
        freq_adj=[-30e6, 40e6],
        band_main=10e6,
        band_adj=[15e6, 15e6])

print(out)

out = dp.acpr_plot(x, fs=300e6,
        freq_main=10e6,
        freq_adj=[-30e6, 40e6, 100e6],
        band_main=10e6,
        band_adj=[25e6])

dp.psd([x, d*2**-15], legend=['1234567890123456', 'b'], show=False, legend_outside=True)
plt.gca().legend()

print(out)

# dp.acpr(x, )
# dp.xy(xy_list)
# x_list = tuple(x_list)

# plt.figure()
# dp.reim(x_list, legend=['a', 'b', 'c'])
# dp.reim(df['x'], legend=['a', 'b', 'c'])
# dp.reim(df['x'].iloc[3:], legend=['a', 'b', 'c'])
# dp.reim(x_list, legend=['a', 'b', 'c'])

# dp.plot([[1,2,3], [4,5,6,7]])

# dp.psd(x_list)
# dp.psd(x_list, nfft=500)



# plt.figure()
# for xi in x_list:
#     plt.psd(xi)
# plt.show()